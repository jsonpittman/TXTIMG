# TXTIMG



## What is TXTIMAGE?

Just a toy. It's a Winforms application that converts an image into a text file, and a Freebasic program that can display the text file in DOS (VGA)

## Folder Descriptions

- DOS: This directory goes on a DOS machine, VM, DosBOX, etc.
  - RUNME.BAT: A batch file that runs a sample (and shows how to build the source file in freebasic)
  - TXTIMG.BAS: FreeBASIC source file
  - TXTIMAGE.EXE: Compiled executable of the source. Run with a text file as a parameter (Eg. "TXTIMG.EXE DW.TXT")
  - *.txt: Sample output
- DOSVGA: A Winforms project that creates the text file from an image. More explination is probably needed.
- Images: Sample images that can be used to create text images for DOS display