﻿namespace DOSVGA
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.butLoad = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.but_CropRD = new System.Windows.Forms.RadioButton();
            this.button8 = new System.Windows.Forms.RadioButton();
            this.but_CropLD = new System.Windows.Forms.RadioButton();
            this.but_CropR = new System.Windows.Forms.RadioButton();
            this.button5 = new System.Windows.Forms.RadioButton();
            this.but_CropL = new System.Windows.Forms.RadioButton();
            this.but_CropRU = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.RadioButton();
            this.but_CropLU = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdoDither_None = new System.Windows.Forms.RadioButton();
            this.rdoDither_Reim = new System.Windows.Forms.RadioButton();
            this.rdoDither_Floyd = new System.Windows.Forms.RadioButton();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdoFit = new System.Windows.Forms.RadioButton();
            this.rdoCrop = new System.Windows.Forms.RadioButton();
            this.txtOutputPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(12, 94);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(640, 480);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(242, 26);
            this.txtPath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(314, 23);
            this.txtPath.TabIndex = 4;
            this.txtPath.Text = "..\\..\\..\\..\\..\\Images\\dw.png";
            // 
            // butLoad
            // 
            this.butLoad.Location = new System.Drawing.Point(561, 26);
            this.butLoad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.butLoad.Name = "butLoad";
            this.butLoad.Size = new System.Drawing.Size(82, 22);
            this.butLoad.TabIndex = 5;
            this.butLoad.Text = "Load";
            this.butLoad.UseVisualStyleBackColor = true;
            this.butLoad.Click += new System.EventHandler(this.butLoad_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.but_CropRD);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.but_CropLD);
            this.groupBox1.Controls.Add(this.but_CropR);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.but_CropL);
            this.groupBox1.Controls.Add(this.but_CropRU);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.but_CropLU);
            this.groupBox1.Location = new System.Drawing.Point(658, 94);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(115, 106);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Crop Direction";
            // 
            // but_CropRD
            // 
            this.but_CropRD.Appearance = System.Windows.Forms.Appearance.Button;
            this.but_CropRD.Image = ((System.Drawing.Image)(resources.GetObject("but_CropRD.Image")));
            this.but_CropRD.Location = new System.Drawing.Point(76, 74);
            this.but_CropRD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.but_CropRD.Name = "but_CropRD";
            this.but_CropRD.Size = new System.Drawing.Size(26, 22);
            this.but_CropRD.TabIndex = 24;
            this.but_CropRD.UseVisualStyleBackColor = true;
            this.but_CropRD.CheckedChanged += new System.EventHandler(this.butCrop_CheckedChanged);
            // 
            // button8
            // 
            this.button8.Appearance = System.Windows.Forms.Appearance.Button;
            this.button8.Image = ((System.Drawing.Image)(resources.GetObject("button8.Image")));
            this.button8.Location = new System.Drawing.Point(45, 74);
            this.button8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(26, 22);
            this.button8.TabIndex = 23;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.CheckedChanged += new System.EventHandler(this.butCrop_CheckedChanged);
            // 
            // but_CropLD
            // 
            this.but_CropLD.Appearance = System.Windows.Forms.Appearance.Button;
            this.but_CropLD.Image = ((System.Drawing.Image)(resources.GetObject("but_CropLD.Image")));
            this.but_CropLD.Location = new System.Drawing.Point(13, 74);
            this.but_CropLD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.but_CropLD.Name = "but_CropLD";
            this.but_CropLD.Size = new System.Drawing.Size(26, 22);
            this.but_CropLD.TabIndex = 22;
            this.but_CropLD.UseVisualStyleBackColor = true;
            this.but_CropLD.CheckedChanged += new System.EventHandler(this.butCrop_CheckedChanged);
            // 
            // but_CropR
            // 
            this.but_CropR.Appearance = System.Windows.Forms.Appearance.Button;
            this.but_CropR.Image = ((System.Drawing.Image)(resources.GetObject("but_CropR.Image")));
            this.but_CropR.Location = new System.Drawing.Point(76, 46);
            this.but_CropR.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.but_CropR.Name = "but_CropR";
            this.but_CropR.Size = new System.Drawing.Size(26, 22);
            this.but_CropR.TabIndex = 21;
            this.but_CropR.UseVisualStyleBackColor = true;
            this.but_CropR.CheckedChanged += new System.EventHandler(this.butCrop_CheckedChanged);
            // 
            // button5
            // 
            this.button5.Appearance = System.Windows.Forms.Appearance.Button;
            this.button5.Checked = true;
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.Location = new System.Drawing.Point(45, 46);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(26, 22);
            this.button5.TabIndex = 20;
            this.button5.TabStop = true;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.CheckedChanged += new System.EventHandler(this.butCrop_CheckedChanged);
            // 
            // but_CropL
            // 
            this.but_CropL.Appearance = System.Windows.Forms.Appearance.Button;
            this.but_CropL.Image = ((System.Drawing.Image)(resources.GetObject("but_CropL.Image")));
            this.but_CropL.Location = new System.Drawing.Point(13, 46);
            this.but_CropL.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.but_CropL.Name = "but_CropL";
            this.but_CropL.Size = new System.Drawing.Size(26, 22);
            this.but_CropL.TabIndex = 19;
            this.but_CropL.UseVisualStyleBackColor = true;
            this.but_CropL.CheckedChanged += new System.EventHandler(this.butCrop_CheckedChanged);
            // 
            // but_CropRU
            // 
            this.but_CropRU.Appearance = System.Windows.Forms.Appearance.Button;
            this.but_CropRU.Image = ((System.Drawing.Image)(resources.GetObject("but_CropRU.Image")));
            this.but_CropRU.Location = new System.Drawing.Point(76, 20);
            this.but_CropRU.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.but_CropRU.Name = "but_CropRU";
            this.but_CropRU.Size = new System.Drawing.Size(26, 22);
            this.but_CropRU.TabIndex = 18;
            this.but_CropRU.UseVisualStyleBackColor = true;
            this.but_CropRU.CheckedChanged += new System.EventHandler(this.butCrop_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Appearance = System.Windows.Forms.Appearance.Button;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(45, 20);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(26, 22);
            this.button2.TabIndex = 17;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.CheckedChanged += new System.EventHandler(this.butCrop_CheckedChanged);
            // 
            // but_CropLU
            // 
            this.but_CropLU.Appearance = System.Windows.Forms.Appearance.Button;
            this.but_CropLU.Image = ((System.Drawing.Image)(resources.GetObject("but_CropLU.Image")));
            this.but_CropLU.Location = new System.Drawing.Point(13, 20);
            this.but_CropLU.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.but_CropLU.Name = "but_CropLU";
            this.but_CropLU.Size = new System.Drawing.Size(26, 22);
            this.but_CropLU.TabIndex = 16;
            this.but_CropLU.UseVisualStyleBackColor = true;
            this.but_CropLU.CheckedChanged += new System.EventHandler(this.butCrop_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdoDither_None);
            this.groupBox2.Controls.Add(this.rdoDither_Reim);
            this.groupBox2.Controls.Add(this.rdoDither_Floyd);
            this.groupBox2.Location = new System.Drawing.Point(658, 212);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(219, 94);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dithering";
            // 
            // rdoDither_None
            // 
            this.rdoDither_None.AutoSize = true;
            this.rdoDither_None.Location = new System.Drawing.Point(10, 69);
            this.rdoDither_None.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdoDither_None.Name = "rdoDither_None";
            this.rdoDither_None.Size = new System.Drawing.Size(54, 19);
            this.rdoDither_None.TabIndex = 2;
            this.rdoDither_None.Text = "None";
            this.rdoDither_None.UseVisualStyleBackColor = true;
            this.rdoDither_None.CheckedChanged += new System.EventHandler(this.butCrop_CheckedChanged);
            // 
            // rdoDither_Reim
            // 
            this.rdoDither_Reim.AutoSize = true;
            this.rdoDither_Reim.Location = new System.Drawing.Point(10, 46);
            this.rdoDither_Reim.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdoDither_Reim.Name = "rdoDither_Reim";
            this.rdoDither_Reim.Size = new System.Drawing.Size(84, 19);
            this.rdoDither_Reim.TabIndex = 1;
            this.rdoDither_Reim.Text = "Riemersma";
            this.rdoDither_Reim.UseVisualStyleBackColor = true;
            this.rdoDither_Reim.CheckedChanged += new System.EventHandler(this.butCrop_CheckedChanged);
            // 
            // rdoDither_Floyd
            // 
            this.rdoDither_Floyd.AutoSize = true;
            this.rdoDither_Floyd.Checked = true;
            this.rdoDither_Floyd.Location = new System.Drawing.Point(10, 24);
            this.rdoDither_Floyd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdoDither_Floyd.Name = "rdoDither_Floyd";
            this.rdoDither_Floyd.Size = new System.Drawing.Size(109, 19);
            this.rdoDither_Floyd.TabIndex = 0;
            this.rdoDither_Floyd.TabStop = true;
            this.rdoDither_Floyd.Text = "Floyd-Steinberg";
            this.rdoDither_Floyd.UseVisualStyleBackColor = true;
            this.rdoDither_Floyd.CheckedChanged += new System.EventHandler(this.butCrop_CheckedChanged);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(562, 65);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(80, 24);
            this.button3.TabIndex = 18;
            this.button3.Text = "Convert";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdoFit);
            this.groupBox3.Controls.Add(this.rdoCrop);
            this.groupBox3.Location = new System.Drawing.Point(778, 94);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(101, 106);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Crop Mode";
            // 
            // rdoFit
            // 
            this.rdoFit.AutoSize = true;
            this.rdoFit.Location = new System.Drawing.Point(10, 49);
            this.rdoFit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdoFit.Name = "rdoFit";
            this.rdoFit.Size = new System.Drawing.Size(38, 19);
            this.rdoFit.TabIndex = 1;
            this.rdoFit.Text = "Fit";
            this.rdoFit.UseVisualStyleBackColor = true;
            this.rdoFit.CheckedChanged += new System.EventHandler(this.butCrop_CheckedChanged);
            // 
            // rdoCrop
            // 
            this.rdoCrop.AutoSize = true;
            this.rdoCrop.Checked = true;
            this.rdoCrop.Location = new System.Drawing.Point(10, 22);
            this.rdoCrop.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdoCrop.Name = "rdoCrop";
            this.rdoCrop.Size = new System.Drawing.Size(51, 19);
            this.rdoCrop.TabIndex = 0;
            this.rdoCrop.TabStop = true;
            this.rdoCrop.Text = "Crop";
            this.rdoCrop.UseVisualStyleBackColor = true;
            this.rdoCrop.CheckedChanged += new System.EventHandler(this.butCrop_CheckedChanged);
            // 
            // txtOutputPath
            // 
            this.txtOutputPath.Location = new System.Drawing.Point(242, 66);
            this.txtOutputPath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtOutputPath.Name = "txtOutputPath";
            this.txtOutputPath.Size = new System.Drawing.Size(314, 23);
            this.txtOutputPath.TabIndex = 20;
            this.txtOutputPath.Text = "..\\..\\..\\..\\..\\DOS\\";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(166, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 15);
            this.label1.TabIndex = 21;
            this.label1.Text = "Image Path:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(156, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 19);
            this.label2.TabIndex = 22;
            this.label2.Text = "Output Path:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(896, 590);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtOutputPath);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.butLoad);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private PictureBox pictureBox1;
        private TextBox txtPath;
        private Button butLoad;
        private GroupBox groupBox1;
        private RadioButton but_CropRD;
        private RadioButton button8;
        private RadioButton but_CropLD;
        private RadioButton but_CropR;
        private RadioButton button5;
        private RadioButton but_CropL;
        private RadioButton but_CropRU;
        private RadioButton button2;
        private RadioButton but_CropLU;
        private GroupBox groupBox2;
        private RadioButton rdoDither_None;
        private RadioButton rdoDither_Reim;
        private RadioButton rdoDither_Floyd;
        private Button button3;
        private GroupBox groupBox3;
        private RadioButton rdoFit;
        private RadioButton rdoCrop;
        private TextBox txtOutputPath;
        private Label label1;
        private Label label2;
    }
}