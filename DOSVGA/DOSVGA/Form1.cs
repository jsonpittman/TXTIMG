using ImageMagick;

namespace DOSVGA
{
    public partial class Form1 : Form
    {
        MagickImage img;
        MagickImage crop_image; //apply crop & dither to a copy of the original
        ImageMagick.MagickImage pal_ref; //vga palette reference image
        List<string> VGA_Colors = new List<string>();
        const int Dos_Width = 640;
        const int Dos_Height = 480;
        public Form1()
        {
            InitializeComponent();
            pal_ref = new ImageMagick.MagickImage("dos256pal.png");
            LoadReference();
        }

        private void butLoad_Click(object sender, EventArgs e)
        {
            string path = Path.GetFullPath(txtPath.Text);
            if(File.Exists(path))
            {
                img = new ImageMagick.MagickImage(txtPath.Text);
                DoCrop();
            }
        }

        private void butCrop_CheckedChanged(object sender, EventArgs e)
        {
            var s = (RadioButton)sender;
            if (s.Checked)
                DoCrop();
        }

        private void DoCrop()
        {
            //MagickImage background_img = new MagickImage(MagickColor.FromRgb(PanelFitBG.BackColor.R, PanelFitBG.BackColor.G, PanelFitBG.BackColor.B), res.Width, res.Height);
            MagickImage background_img = new MagickImage(MagickColor.FromRgb(0, 0, 0), Dos_Width, Dos_Height);
            // a copy of the original image to crop
            crop_image = new MagickImage(img);
            decimal aspect = crop_image.Height / (decimal)Dos_Height;

            if (rdoCrop.Checked) //crop image
            {
                crop_image.Resize((int)(crop_image.Width / aspect), (int)(crop_image.Height / aspect));
                int leftover = (crop_image.Width - Dos_Width) / 2;

                if (but_CropL.Checked || but_CropLU.Checked || but_CropLD.Checked)
                    leftover = 0;
                else if (but_CropR.Checked || but_CropRU.Checked || but_CropRD.Checked)
                    leftover = crop_image.Width - Dos_Width;

                MagickGeometry CropGeom = new MagickGeometry(leftover, 0, Dos_Width, Dos_Height);
                crop_image.Crop(CropGeom);
            }
            else //fit image
            {
                crop_image.Resize(640, (int)(crop_image.Height / aspect));
                int top = (int)((Dos_Height - crop_image.Height) / 2);
                background_img.Composite(crop_image, 0, top);

                crop_image = new MagickImage(background_img);
            }

            // dither
            QuantizeSettings settings = new QuantizeSettings();
            if (rdoDither_Floyd.Checked)
                settings.DitherMethod = DitherMethod.FloydSteinberg;
            else if (rdoDither_Reim.Checked)
                settings.DitherMethod = DitherMethod.Riemersma;
            else
                settings.DitherMethod = DitherMethod.No;

            crop_image.Map(pal_ref, settings);

            pictureBox1.Image = crop_image.ToBitmap();
        }

        private void LoadReference()
        {
            // loads and array of VGA colors from a reference image

            for (int y = 3; y < pal_ref.Height; y+=6)
            {
                for (int x = 3; x < pal_ref.Width; x+=6)
                {
                    string colr = pal_ref.GetPixels().GetPixel(x, y).ToColor().ToHexString();
                        VGA_Colors.Add(colr);
                }
            }
        }

        private void WriteOutput(string fname)
        {

            string line = "";
            List<string> lines = new List<string>();

            for (int y = 0; y < crop_image.Height; y++)
            {
                for (int x = 0; x < crop_image.Width; x++)
                {
                    var clr = crop_image.GetPixels().GetPixel(x, y).ToColor().ToHexString();
                    int vga_index = VGA_Colors.IndexOf(clr);

                    string vga_index_to_hex = vga_index.ToString("X").PadLeft(2, '0');
                    line += vga_index_to_hex;

                    if (line.Length >= 100)
                    {
                        lines.Add(line);
                        line = "";
                    }
                }
            }

            if (line.Length > 0)
                lines.Add(line);

            StreamWriter sw = new StreamWriter(fname, false);
            foreach (string l in lines)
                sw.WriteLine(l);
            sw.Close();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            string outputPath = Path.Combine(
                Path.GetFullPath(txtOutputPath.Text),
                string.Format("{0}.txt", Path.GetFileNameWithoutExtension(txtPath.Text))
            );
            WriteOutput(outputPath);
        }
    }
}